console.log("Hello world!");

// [SECTION] Functions
// functions in javascript are lines/block of codes that tell our devices or application to perform a certain task when called/invoked.
// They are also used to prevent repeating lines/block of codes that perform the same task/function.

//Function Declaration
    //function statement defines a function with parameters
// function keyword - use to define a javascript function
// function declaration
// function name requires open and close parentheis besided it

function printName(){ //code block
    console.log("My name is John"); //function statement
};

printName();
console.log(printName);


// [HOISTING]
// is javascript's behavior for certain variables and functions to run or use before their declaration
declaredFunction();


function declaredFunction(){
    console.log("Hello World!");
}


// [Function Expression]
// a function can be also stored in a variable. that is called as function expression
// cannot run before the declaration


let variableFunction = function(){
    console.log("Hello again!");
};


variableFunction();

let funcExpression = function funcName(){
    console.log("Hello from the other side");
};

funcExpression();
console.log("-------------")
console.log("[Reassignin functions]");


declaredFunction();


declaredFunction = function(){
    console.log("updated declaration");
}

declaredFunction();

// Constant function
const constantFunction = function(){
    console.log("Initialized with const.");
};

constantFunction();


console.log("-----------");
console.log("[Function Scoping]");

/*type of scope
    1. local scope
    2. global scope
    3. function scope
*/

{
    let localVar = "Armando Perez";
console.log(localVar); // cannot run outside codeblock
};



let globalVar = "Mr. Worldwide";
console.log(globalVar); //can access outside and inside codeblock
    {

        console.log(globalVar);
    }


// function scoping

function showNames(){
    var functionVar = "joe";
    const functionConst = "john";
    let functionLet ="jane";

console.log(functionVar);
console.log(functionConst);
console.log(functionLet);

}

showNames();
console.log("-----------")

function myNewFunction(){
    let name = "jane";

function nestedFunction(){
    let nestedName ="John";
    console.log(name);
    console.log(nestedName);

}
nestedFunction();

}

myNewFunction();

console.log("-----------")


// Global Scoped Variable
let globalName = "Zuitt";

function myNewFunction2(){
    let nameInside = "Renz";
    console.log(globalName);
}

myNewFunction2();




function showSampleAlert(){
    alert("Hello, User");
}

showSampleAlert();

//alert messages inside a function will only excute whenever we call/invoke the function

console.log("I will only log in the console when the aleart is dismissed");


// notes on the use of alert();
// show only an alert for short dialogs/messages to the user
// Do not overuse alert() because the program/js has to wait for it be dismissed before continuing.

// [Prompt]

    // prompt() allow us to show small window at the top of the browser to gather user input.

    let samplePrompt = prompt("Enter you full name:")
    //console.log(samplePrompt);

    console.log(typeof samplePrompt); // all entered to prompt are entered to string

    //console.log("Hello, " + samplePrompt);

    function printWelcomMessage(){
        let firstName = prompt("enter your first name:");
            let lastName = prompt("enter your last name:");
            console.log("hello," + firstName + lastName);
    }

printWelcomMessage();


function getCourses(){
    let Courses = ["science 101", "math 101"];
    console.log(Courses);
}

getCourses();

function displayCarInfo(){
    console.log("brand: toyota");
    console.log("type: seda");
    console.log("price: 1,500,000");

}

displayCarInfo();
